import React, { Component } from 'react';
import './App.css';
import {Grid,Row,Col,FormGroup,FormControl,ListGroup,ListGroupItem,Button,ControlLabel} from "react-bootstrap";
var client = require('./client.js');
var moment = require('moment');

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: "",
      notes: []
    };
  }
  successDB(){

  }
  errorDB(){

  }
  componentWillMount(){
    this.loadNote();
  }
  loadNote(){
    client.getRequest('note')
      .then((results) => {
        this.setState({
          notes: results
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }
  saveNote(){
    client.postRequest('note',{content:this.state.content});
    this.loadNote();
  }
  selectNote(){

  }
  render() {
    var list = [];
    for(let i=0;i<this.state.notes.length;i++){
      list.push(<ListGroupItem bsClass="listItem" header={this.state.notes[i].content.substring(0,50)} key={this.state.notes[i].id}>{this.state.notes[i].created.substring(0,10)}</ListGroupItem>);
    }
    return (
      <Grid>
          <Row className="show-grid">
            <Col md={4}>
              <ListGroup id="list">{list}</ListGroup>
            </Col>
            <Col md={8} id="writepad">
              <form>
                <FormGroup>
                  <ControlLabel id="date">{moment().format('Do MMMM, h:mm a')}</ControlLabel>
                  <FormControl componentClass="textarea" id="textarea" value={this.state.content} onChange={(event) => {this.setState({content: event.target.value})}}/>
                  <Button onClick={this.saveNote.bind(this)}>Default</Button>
                </FormGroup>
              </form>
            </Col>
          </Row>
      </Grid>
    );
  }
}

export default App;
